import puppeteer from 'puppeteer';

// Launch the browser and open a new blank page
const browser = await puppeteer.launch();
const page = await browser.newPage();

const contentHtml = fs.readFileSync('/files/resume.html', 'utf8');
await page.setContent(contentHtml);

// Set screen size.
await page.setViewport({width: 1080, height: 1024});

await page.pdf({ 
  path: '/files/resume.pdf', 
  format: 'A4',
  tagged: true,
  printBackground: true,
});


await browser.close();