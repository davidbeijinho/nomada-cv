#!/usr/bin/env bash

podman \
run \
-i \
--init \
--cap-add=SYS_ADMIN \
--rm \
-v $(pwd)/files:/files \
ghcr.io/puppeteer/puppeteer:latest \
node \
--input-type module \
-e \
"$(cat ./scripts/toPdf.mjs)"
