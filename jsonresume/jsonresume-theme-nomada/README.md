# JSON Resume Nomada Theme
 This is a theme for [JSON resume](https://jsonresume.org/): an awesome open source project for generating uniform resumes, with free hosting and customizable themes.
 
## Contribute

This project uses [Handlebars](https://handlebarsjs.com/) and [TailwindCSS](https://tailwindcss.com/)
